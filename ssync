#!/bin/bash

shopt -s -o nounset

# Globals
declare -r SCRIPT=${0##*/}
declare -r VERSION="0.0.1"
declare -r COMPANY="Sloth Company"
declare -r YEAR="2013"
declare -r CONFIG="$HOME/.slc/$SCRIPT/$SCRIPT.conf"
declare -r GETOPT="/usr/bin/getopt"
declare -r LFTP="/usr/bin/lftp"
declare -r ERR=192
declare -r O_SHLP="h"
declare -r O_LHLP="help"
declare -r O_SVRB="v"
declare -r O_LVRB="verbose"
declare -r O_SYES="y"
declare -r O_LYES="yes"
declare -r O_SUPL="U"
declare -r O_LUPL="upload"
declare -r O_SDEL="D"
declare -r O_LDEL="delete"
declare -r O_SUSR="u"
declare -r O_LUSR="user"
declare -r O_SPSW="p"
declare -r O_LPSW="pass"
declare -r O_SRMT="r"
declare -r O_LRMT="remote"
declare -r O_SEXC="e"
declare -r O_LEXC="exclude"
declare -r M_EBASH="$SCRIPT: Please run this script with the 'bash' shell."
declare -r M_ECMD="$SCRIPT: The command %s is not available."
declare -r M_EMISSARGS="$SCRIPT: Missing required argument 'remote-dir'."
declare -r M_ETOOMUCH="$SCRIPT: Too much arguments."
declare -r M_HHLP="Display this help and exit."
declare -r M_HVRB="Display additional messages during execution."
declare -r M_HUPL="Enable upload mode: update server instead of local host."
declare -r M_HYES="Automatic 'y' answer."
declare -r M_HDEL="Delete destination's files not present in source."
declare -r M_HUSR="User name for remote FTP connection."
declare -r M_HPSW="Password for remote FTP connection."
declare -r M_HRMT="Remote host name."
declare -r M_HEXC="Exclude a specific directory from sync."
declare -r M_CNF="Checking configuration..."
declare -r M_CNFFOUND="Found configuration file $CONFIG."
declare -r M_CNFPARS="Parsing configuration..."
declare -r M_CNFERR="Configuration file $CONFIG not found."
declare -r M_CNFMISS="Missing configuration file and %s not defined from command line."
declare -r M_CNFUSR="User from command line: '%s'."
declare -r M_CNFUSRERR="No user from command line. Using configured user '%s'."
declare -r M_CNFPSW="Password from command line: '%s'."
declare -r M_CNFPSWERR="No password from command line. Using configured password '%s'."
declare -r M_CNFRMT="Remote from command line: '%s'."
declare -r M_CNFRMTERR="No remote from command line. Using configured remote '%s'."
declare -r M_CNFEXC="Excludes from command line: '%s'."
declare -r M_CNFEXCERR="No excludes from command line. Using configured excludes '%s'."
declare -r M_RMT="Checking remote directory..."
declare -r M_RMTERR="Remote dir %s needs to be relative to root."
declare -r M_LOC="Checking local directory..."
declare -r M_LOCFOUND="Using directory %s from command line."
declare -r M_LOCPWD="No local directory from command line. Using working directory %s."
declare -r M_LOCERR="%s is not a valid directory."
declare -r M_EXC="Filtering excludes..."
declare -r M_EXCFOUND="Found exclude %s."
declare -r M_EXCSUB="Exclude %s is subdirectory of local directory %s."
declare -r M_SYNC="Starting syncing..."
declare -r M_SYNCCHK="Sync data from %s directory %s to %s directory %s? [y/n]:"
declare -r M_SYNCWRONGANSWER="Please answer with either 'y' or 'n':"
declare -r M_SYNCSTART="Starting sync..."
declare -r M_SYNCEND="Sync completed."
declare FLAG_HELP=false
declare FLAG_VERBOSE=false
declare FLAG_YES=false
declare FLAG_UPLOAD=false
declare FLAG_DELETE=false
declare USER=""
declare PASS=""
declare REMOTE=""
declare -a EXCLUDES=()
declare LOCAL_DIR=""
declare REMOTE_DIR=""

# Functions
function displayHelp {
    local FORMAT=" %-20s: %s\n"
    printf "Usage: $SCRIPT [options] remote-dir [local-dir]\n"
    printf "Options:\n"
    printf "$FORMAT" "-$O_SHLP, --$O_LHLP" "$M_HHLP"
    printf "$FORMAT" "-$O_SVRB, --$O_LVRB" "$M_HVRB"
    printf "$FORMAT" "-$O_SYES, --$O_LYES" "$M_HYES"
    printf "$FORMAT" "-$O_SUPL, --$O_LUPL" "$M_HUPL"
    printf "$FORMAT" "-$O_SDEL, --$O_LDEL" "$M_HDEL"
    printf "$FORMAT" "-$O_SUSR, --$O_LUSR=name" "$M_HUSR"
    printf "$FORMAT" "-$O_SPSW, --$O_LPSW=name" "$M_HPSW"
    printf "$FORMAT" "-$O_SRMT, --$O_LRMT=name" "$M_HRMT"
    printf "$FORMAT" "-$O_SEXC, --$O_LEXC=name" "$M_HEXC"
}

# Sanity checks
[[ -z $BASH ]] && printf "$M_EBASH\n" >&2 && exit $ERR
[[ ! -x $GETOPT ]] && printf "$M_ECMD\n" "$GETOPT" >&2 && exit $ERR
[[ ! -x $LFTP ]] && printf "$M_ECMD\n" "$LFTP" >&2 && exit $ERR

# Manage options
SHORT_OPTS="$O_SHLP$O_SVRB$O_SUPL$O_SDEL$O_SUSR:$O_SPSW:$O_SRMT:$O_SEXC:"
LONG_OPTS="$O_LHLP,$O_LVRB,$O_LUPL,$O_LDEL,$O_LUSR:,$O_LPSW:,$O_LRMT:,$O_LEXC:"
OPTS=$(getopt -n "$SCRIPT" -o $SHORT_OPTS -l $LONG_OPTS -- "$@")
[[ $? -ne 0 ]] && displayHelp && exit $ERR
eval set -- "$OPTS"

#EXCLUDES_FIRST=true
while true
do
    case "$1" in
	-$O_SHLP|--$O_LHLP)
    	FLAG_HELP=true
        shift;;
	-$O_SVRB|--$O_LVRB)
        FLAG_VERBOSE=true
        shift;;
	-$O_SUPL|--$O_LUPL)
        FLAG_UPLOAD=true
        shift;;
	-$O_SDEL|--$O_LDEL)
        FLAG_DELETE=true
        shift;;
	-$O_SUSR|--$O_LUSR)
        USER="$2"
        shift 2;;
	-$O_SPSW|--$O_LPSW)
        PASS="$2"
        shift 2;;
	-$O_SRMT|--$O_LRMT)
        REMOTE="$2"
        shift 2;;
	-$O_SEXC|--$O_LEXC)
	    COUNT_EXC=0
		[[ -n ${EXCLUDES[@]:+${EXCLUDES[@]}} ]] && COUNT_EXC=${#EXCLUDES[@]}
        EXCLUDES[$COUNT_EXC]="$2"
        shift 2;;
    --)
        shift
        break;;
    esac
done

# Check number of arguments
[[ $# -lt 1 ]] && printf "$M_EMISSARGS\n" && displayHelp && exit $ERR
[[ $# -gt 2 ]] && printf "$M_ETOOMUCH\n" && displayHelp && exit $ERR

# Help
$FLAG_HELP && printf "$SCRIPT $VERSION\n$COMPANY $YEAR\n\n" && displayHelp && exit 0


# Config
$FLAG_VERBOSE && printf "$M_CNF\n"
user=""
pass=""
remote=""
exclude=""

if [[ -r $CONFIG ]]; then
	$FLAG_VERBOSE && printf "  $M_CNFFOUND\n"
	source $CONFIG
else
	$FLAG_VERBOSE && printf "  $M_CNFERR\n"
fi
$FLAG_VERBOSE && printf "$M_CNFPARS\n"

if [[ -z $USER ]]; then
	[[ ! -r $CONFIG ]] && printf "  $M_CNFMISS\n" "user" && exit $ERR
	USER=$user
	$FLAG_VERBOSE && printf "  $M_CNFUSRERR\n" "$USER"
else
	$FLAG_VERBOSE && printf "  $M_CNFUSR\n" "$USER"
fi

if [[ -z $PASS ]]; then
	[[ ! -r $CONFIG ]] && printf "  $M_CNFMISS\n" "password" && exit $ERR
	PASS=$pass
	$FLAG_VERBOSE && printf "  $M_CNFPSWERR\n" "$PASS"
else
	$FLAG_VERBOSE && printf "  $M_CNFPSW\n" "$PASS"
fi

if [[ -z $REMOTE ]]; then
	[[ ! -r $CONFIG ]] && printf "  $M_CNFMISS\n" "remote" && exit $ERR
	REMOTE=$remote
	$FLAG_VERBOSE && printf "  $M_CNFRMTERR\n" "$REMOTE"
else
	$FLAG_VERBOSE && printf "  $M_CNFRMT\n" "$REMOTE"
fi

if [[ ! -n ${EXCLUDES[@]:+${EXCLUDES[@]}} ]]; then
	IFS=', '
	excludesString=''
	read -a EXCLUDES <<< "$exclude"
	[[ -n ${EXCLUDES[@]:+${EXCLUDES[@]}} ]] && excludesString="${EXCLUDES[@]}"
	$FLAG_VERBOSE && printf "  $M_CNFEXCERR\n" "$excludesString"
else
	$FLAG_VERBOSE && printf "  $M_CNFEXC\n" "${EXCLUDES[@]}"
fi

# Remote dir
$FLAG_VERBOSE && printf "$M_RMT\n"
REMOTE_DIR="$1"
[[ ${REMOTE_DIR:0:1} != '/' ]] && printf "  $M_RMTERR\n" $REMOTE_DIR && exit $ERR

# Check if local directory exists
$FLAG_VERBOSE && printf "$M_LOC\n"
if [ $# -eq 2 ]; then
	LOCAL_DIR="$2"
	$FLAG_VERBOSE && printf "  $M_LOCFOUND\n" $LOCAL_DIR
else
	LOCAL_DIR=`pwd`
	$FLAG_VERBOSE && printf "  $M_LOCPWD\n" $LOCAL_DIR
fi

[[ ! -d $LOCAL_DIR ]] && printf "  $M_LOCERR\n" $LOCAL_DIR && exit $ERR

# Filter excludes that aren't children of the local directory
$FLAG_VERBOSE && printf "$M_EXC\n"
EXCLUDE_STRING=""
if [[ -n ${EXCLUDES[@]:+${EXCLUDES[@]}} ]]; then
	for EXCLUDE in ${EXCLUDES[@]}; do
		$FLAG_VERBOSE && printf "  $M_EXCFOUND\n" $EXCLUDE
	    if [ -d $LOCAL_DIR/$EXCLUDE ]; then
	    	$FLAG_VERBOSE && printf "  $M_EXCSUB\n" $EXCLUDE $LOCAL_DIR
	    	EXCLUDE_STRING="$EXCLUDE_STRING --exclude $EXCLUDE"
	    fi
	done
fi

# Sync through lftp
$FLAG_VERBOSE && printf "$M_SYNC\n"

localString=$LOCAL_DIR

realRemoteDir=$REMOTE_DIR
$FLAG_UPLOAD && [[ ${REMOTE_DIR:${#REMOTE_DIR}-1:1} == '/' ]] && realRemoteDir=$REMOTE_DIR$(basename $LOCAL_DIR)
remoteString="ftp://$USER:$PASS@$REMOTE$realRemoteDir"

UPLOAD_OPT=""
DELETE_OPT=""
DIR_FROM=$REMOTE_DIR
DIR_TO=$LOCAL_DIR
dirFromString=$remoteString
dirToString=$localString
if $FLAG_UPLOAD; then
    UPLOAD_OPT="R"
    DIR_FROM=$LOCAL_DIR
    DIR_TO=$REMOTE_DIR
    dirFromString=$localString
    dirToString=$remoteString
fi
$FLAG_DELETE && DELETE_OPT="e"

if ! $FLAG_YES; then
	fromType="remote"
	toType="local"
	$FLAG_UPLOAD && fromType="local" && toType="remote"
	printf "$M_SYNCCHK " $fromType $dirFromString $toType $dirToString
	validAnswer=false
	while ! $validAnswer ; do
		read answer
		validAnswer=true
		[[ $answer == 'n' ]] && exit 0
		[[ $answer != 'y' ]] && printf "$M_SYNCWRONGANSWER " && validAnswer=false
	done
fi
printf "$M_SYNCSTART\n"

lftp -u $USER,$PASS $REMOTE <<EOF
set ftp:ssl-allow no
mirror -c$DELETE_OPT$UPLOAD_OPT $DIR_FROM $DIR_TO $EXCLUDE_STRING
EOF

printf "$M_SYNCEND\n"